## Sam's Kodi Addon Repository
(not endorsed by The Kodi team)


### To Use

Download the zip file above then use ["Install add-on from zip file"](http://kodi.wiki/view/HOW-TO:Install_add-ons_from_zip_files) in Kodi to install. The repository will be added to the "Install from repository" menu in Kodi's Add-on browser.


### Branches
 * **[master](https://bitbucket.org/bailus/kodi-addons/src/production/):** This branch. A handy place to put a readme and a zip file.
 * **[staging](https://bitbucket.org/bailus/kodi-addons/src/production/):** Addons are added here as directories full of files. [Bitbucket Pipelines](https://bitbucket.org/product/features/pipelines) automatically generates the **production** branch from these.
 * **[production](https://bitbucket.org/bailus/kodi-addons/src/production/):** The zip file in the **master** branch tells Kodi to use this Bitbucket branch as an addon repository. It contains the files from the **staging** branch, along with generated zip, md5 and xml files.
 